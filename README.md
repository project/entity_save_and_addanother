# Entity Save And Add Another

The Entity Create Another Button module for Drupal 10 allows you to add a button
on an entity form that enables the creation of a new entity content of the same
type seamlessly.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_save_and_addanother).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_save_and_addanother).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module does not have any dependencies on other modules.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not require any additional configuration. Once installed, the
functionality will be available on entity forms.


## Maintainers

- Neslee Canil Pinto - [neslee-canil-pinto](https://www.drupal.org/u/neslee-canil-pinto)
